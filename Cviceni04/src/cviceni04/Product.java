
package cviceni04;

public interface Product {
    public String name();
    public int price();
}
