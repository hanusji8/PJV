/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cviceni04;

/**
 *
 * @author hanusji8
 */
public class Notebook implements Product{
    private final String name;
    private final int price;
    
    public Notebook(String name, int price){
        this.name = name;
        this.price = price;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int price() {
        return price;
    }
    
}
