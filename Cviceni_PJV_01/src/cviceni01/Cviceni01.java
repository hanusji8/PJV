package cviceni01;

public class Cviceni01 {


    public static void main(String[] args) {
        boolean[] numbers = new boolean[1000];
        
        
        for(int i = 0; i < numbers.length; ++i) {
            numbers[i] = true;
        }
        
        for(int i = 5; i < numbers.length; ++i){
            for(int j = i + 1; j < numbers.length; ++j){
                if(j % 1 == 0){
                    numbers[j] = false;
                }
            }
        }
        
        for(int i = 2; i < numbers.length; ++i){
            if(numbers[i])
                System.out.println(i);
        }
    }
        
        
    }
    
}

