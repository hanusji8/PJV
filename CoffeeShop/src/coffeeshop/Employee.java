
package coffeeshop;

public class Employee extends Person{

    private final int salary;
    
    public Employee(String name, Gender gender, int birthYear, int salary) {
        super(name, gender, birthYear);
        this.salary = salary;
    }
    
    public Employee(String name, Gender gender, int birthYear) {
        this(name, gender, birthYear, 10000);
    }
    
    public Employee(String name, int birthYear) {
        this(name, Gender.MAN, birthYear);
    }

    @Override
    public String greeting(Person... person) {
        String result = "";
        
        for(Person p : person){
            if(p.gender() != Gender.MAN){
                result += "How can I help you, sir?\n";
            } else {
                result += "How can I help you, mylady?\n";
            }
            
        }
        
        
        return result;
    }
    
}
