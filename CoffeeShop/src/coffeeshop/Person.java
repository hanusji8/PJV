package coffeeshop;

import java.util.Calendar;

public abstract class Person {
  private final String name;
  private final Gender gender;
  private final int birthYear;
  
  protected Person(String name, Gender gender, int birthYear) {
    this.name = name;
    this.gender = gender;
    this.birthYear = birthYear;
  }

  public String name() {
    return name;
  }

  public Gender gender() {
    return gender;
  }

  
  
  public int birthYear() {
    return birthYear;
  }
  
  public int age() {
    return Calendar.getInstance().get(Calendar.YEAR) - birthYear;
  }
  
  public abstract String greeting(Person... person);
}
