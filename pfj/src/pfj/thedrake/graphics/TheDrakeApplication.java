package pfj.thedrake.graphics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TheDrakeApplication extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("template.fxml"));
        
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("pfj/thedrake/graphics/style.css");
        stage.setMinHeight(600);
        stage.setHeight(600);
        stage.setMinWidth(800);
        stage.setWidth(800);
        stage.setScene(scene);
        stage.setTitle("The Drake");
        stage.show();

    }
    
    public static void main(String [] args){
        launch(args);
    }
}
