package pfj.thedrake.graphics;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public class TheDrakeController implements Initializable{
    
    @FXML private javafx.scene.control.Button closeButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    public void closeButton(ActionEvent event){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}
