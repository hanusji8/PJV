package pfj.thedrake.media;

import pfj.thedrake.game.EmptyTile;
import pfj.thedrake.game.TroopTile;

public interface TileMedia<T> {
	public T putTroopTile(TroopTile tile);	
	public T putEmptyTile(EmptyTile tile);
}
