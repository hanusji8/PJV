
package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;
import pfj.thedrake.game.BothLeadersPlaced;
import pfj.thedrake.game.NoLeadersPlaced;
import pfj.thedrake.game.OneLeaderPlaced;
import pfj.thedrake.game.PlayingSide;
import pfj.thedrake.media.LeadersMedia;
import pfj.thedrake.media.PrintMedia;


public class LeadersPlainTextMedia extends PrintMedia implements LeadersMedia<Void>{
    public LeadersPlainTextMedia(OutputStream stream) {
        super(stream);
    }


    @Override
    public Void putNoLeadersPlaced(NoLeadersPlaced leaders) {
        writer().println("NL");
        return null;
    }

    @Override
    public Void putOneLeaderPlaced(OneLeaderPlaced leaders) {
        PrintWriter w = writer();
        w.printf("OL ");
        
        if(leaders.isPlaced(PlayingSide.BLUE)){
            w.printf(leaders.position(PlayingSide.BLUE).toString());
        }
        else{
            w.printf(leaders.position(PlayingSide.ORANGE).toString());
        }
        w.printf("\n");
        
        return null;
    }

    @Override
    public Void putBothLeadersPlaced(BothLeadersPlaced leaders) {
        writer().printf("BL %s %s\n", leaders.position(PlayingSide.BLUE), 
                        leaders.position(PlayingSide.ORANGE));
        return null;
    }


  


}
