/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;
import pfj.thedrake.game.BasicTroopStacks;
import pfj.thedrake.game.PlayingSide;
import pfj.thedrake.game.TroopInfo;
import pfj.thedrake.media.PrintMedia;
import pfj.thedrake.media.TroopStacksMedia;

/**
 *
 * @author hany
 */
public class TroopStacksPlainTextMedia extends PrintMedia implements TroopStacksMedia<Void>{

    public TroopStacksPlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putBasicTroopStacks(BasicTroopStacks stacks) {
        PrintWriter w = writer();
        
        w.print("BLUE stack:");
        for(TroopInfo it : stacks.troops(PlayingSide.BLUE)){
            w.printf(" " + it.name());
        }
        w.printf("\n");
        
        w.print("ORANGE stack:");
        for(TroopInfo it : stacks.troops(PlayingSide.ORANGE)){
            w.printf(" " + it.name());
        }
        w.printf("\n");
  
        return null;
    }

    
    
    
}
