
package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;
import pfj.thedrake.game.EmptyTile;
import pfj.thedrake.game.Troop;
import pfj.thedrake.game.TroopTile;
import pfj.thedrake.media.PrintMedia;
import pfj.thedrake.media.TileMedia;


public class TilePlainTextMedia extends PrintMedia implements TileMedia<Void>{

    public TilePlainTextMedia(OutputStream stream){
        super(stream);
    }
    
    @Override
    public Void putTroopTile(TroopTile tile) {
        PrintWriter w = writer();
        Troop troop = tile.troop();

        w.printf(troop.info().name() + " " + troop.side() + " " + troop.face() + "\n");
        return null;

    }

    @Override
    public Void putEmptyTile(EmptyTile tile) {
        PrintWriter w = writer();
        
        w.printf("empty\n");
        return null;
    }

   
}
