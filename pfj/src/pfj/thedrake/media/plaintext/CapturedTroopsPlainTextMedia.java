/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;
import pfj.thedrake.game.CapturedTroops;
import pfj.thedrake.game.PlayingSide;
import pfj.thedrake.game.TroopInfo;
import pfj.thedrake.media.CapturedTroopsMedia;
import pfj.thedrake.media.PrintMedia;

/**
 *
 * @author hany
 */
class CapturedTroopsPlainTextMedia extends PrintMedia implements CapturedTroopsMedia<Void>{

    
    CapturedTroopsPlainTextMedia(OutputStream stream) {
        super(stream);
    }

    @Override
    public Void putCapturedTroops(CapturedTroops captured) {
        PrintWriter w = writer();

        w.printf("Captured BLUE: " + captured.troops(PlayingSide.BLUE).size() + "\n");
        for(TroopInfo info : captured.troops(PlayingSide.BLUE)){
            w.printf(info.name() + "\n");
        }
        
        w.printf("Captured ORANGE: " + captured.troops(PlayingSide.ORANGE).size());
        for(TroopInfo info : captured.troops(PlayingSide.ORANGE)){
            w.printf("\n" + info.name());
        }
    
        

        return null;

    }

    
}
