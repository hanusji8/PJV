/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;
import pfj.thedrake.game.MiddleGameState;
import pfj.thedrake.game.PlacingGuardsGameState;
import pfj.thedrake.game.PlacingLeadersGameState;
import pfj.thedrake.game.PlayingSide;
import pfj.thedrake.game.VictoryGameState;
import pfj.thedrake.media.GameStateMedia;
import pfj.thedrake.media.PrintMedia;

/**
 *
 * @author hany
 */
public class GameStatePlainTextMedia extends PrintMedia implements GameStateMedia<Void>{
    
    private final TroopStacksPlainTextMedia troopStacksPlainTextMedia;
    private final BoardPlainTextMedia boardPlainTextMedia;
    private final LeadersPlainTextMedia leadersPlainTextMedia;
    
    public GameStatePlainTextMedia(OutputStream stream){
        super(stream);
        this.troopStacksPlainTextMedia = new TroopStacksPlainTextMedia(stream);
        this.boardPlainTextMedia = new BoardPlainTextMedia(stream);
        this.leadersPlainTextMedia = new LeadersPlainTextMedia(stream);
    }

    
    @Override
    public Void putPlacingLeadersGameState(PlacingLeadersGameState state){
        PrintWriter w = writer();
        
        w.printf("LEADERS\n");
        w.printf("0\n");
        w.printf(state.sideOnTurn().name() + "\n");
        state.troopStacks().putToMedia(this.troopStacksPlainTextMedia);
        state.leaders().putToMedia(this.leadersPlainTextMedia);
        state.board().putToMedia(this.boardPlainTextMedia);
        
       
        return null;
    }


    @Override
    public Void putPlacingGuardsGameState(PlacingGuardsGameState state) {
        PrintWriter w = writer();

        w.println("GUARDS");
        w.printf(state.guardsCount() + "\n");
        w.printf(state.sideOnTurn().name() + "\n");
        state.troopStacks().putToMedia(this.troopStacksPlainTextMedia);
        state.leaders().putToMedia(this.leadersPlainTextMedia);
        state.board().putToMedia(this.boardPlainTextMedia);
        
        
        return null;

    }

    @Override
    public Void putMiddleGameState(MiddleGameState state) {
        PrintWriter w = writer();

        w.printf("MIDDLE\n");
        w.printf("4\n");
        w.printf(state.sideOnTurn().name() + "\n");
        state.troopStacks().putToMedia(this.troopStacksPlainTextMedia);
        state.leaders().putToMedia(this.leadersPlainTextMedia);
        state.board().putToMedia(this.boardPlainTextMedia);
        
        return null;

    }

    @Override
    public Void putFinishedGameState(VictoryGameState state) {
        PrintWriter w = writer();

        w.println("VICTORY");
        w.printf("4\n");
        w.printf(state.sideOnTurn().name() + "\n");
        state.troopStacks().putToMedia(this.troopStacksPlainTextMedia);
        if(state.leaders().isPlaced(PlayingSide.BLUE)){
            w.printf("OL " + state.leaders().position(PlayingSide.BLUE) + " X\n");
        }
        else{
            w.printf("OL X " + state.leaders().position(PlayingSide.ORANGE) + "\n");
        }
        state.board().putToMedia(this.boardPlainTextMedia);
        
        return null;

    }
    
}
