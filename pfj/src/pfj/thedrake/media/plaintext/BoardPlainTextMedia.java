package pfj.thedrake.media.plaintext;

import java.io.OutputStream;
import java.io.PrintWriter;

import pfj.thedrake.game.Board;
import pfj.thedrake.game.Tile;
import pfj.thedrake.media.BoardMedia;
import pfj.thedrake.media.PrintMedia;
import pfj.thedrake.media.TileMedia;

public class BoardPlainTextMedia extends PrintMedia implements BoardMedia<Void> {
	private final TilePlainTextMedia tileMedia;
	private final CapturedTroopsPlainTextMedia capturedMedia;
	
	public BoardPlainTextMedia(OutputStream stream) {
		super(stream);
		this.tileMedia = new TilePlainTextMedia(stream);
		this.capturedMedia = new CapturedTroopsPlainTextMedia(stream);
	}
	
	@Override
	public Void putBoard(Board board) {
		PrintWriter w = writer();

		w.println(board.dimension());
		for(Tile tile : board) {
			tile.putToMedia(tileMedia);
		}
		
		capturedMedia.putCapturedTroops(board.captured());
		
		return null;
	}
}
