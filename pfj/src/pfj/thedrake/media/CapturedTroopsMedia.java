package pfj.thedrake.media;

import pfj.thedrake.game.CapturedTroops;

public interface CapturedTroopsMedia<T> {
	public T putCapturedTroops(CapturedTroops captured);
}
