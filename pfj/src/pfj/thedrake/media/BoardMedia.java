package pfj.thedrake.media;

import pfj.thedrake.game.Board;

public interface BoardMedia<T> {
	public T putBoard(Board board);
}
