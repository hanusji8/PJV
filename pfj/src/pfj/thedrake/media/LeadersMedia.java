package pfj.thedrake.media;

import pfj.thedrake.game.BothLeadersPlaced;
import pfj.thedrake.game.NoLeadersPlaced;
import pfj.thedrake.game.OneLeaderPlaced;

public interface LeadersMedia<T> {
	public T putNoLeadersPlaced(NoLeadersPlaced leaders);
	public T putOneLeaderPlaced(OneLeaderPlaced leaders);
	public T putBothLeadersPlaced(BothLeadersPlaced leaders);
}
