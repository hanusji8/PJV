package pfj.thedrake.media;

import pfj.thedrake.game.BasicTroopStacks;

public interface TroopStacksMedia<T> {
	public T putBasicTroopStacks(BasicTroopStacks stacks);
}
