
package pfj.thedrake.media;

import pfj.thedrake.game.MiddleGameState;
import pfj.thedrake.game.PlacingGuardsGameState;
import pfj.thedrake.game.PlacingLeadersGameState;
import pfj.thedrake.game.VictoryGameState;

public interface GameStateMedia<T> {
	public T putPlacingLeadersGameState(PlacingLeadersGameState state);
	public T putPlacingGuardsGameState(PlacingGuardsGameState state);
	public T putMiddleGameState(MiddleGameState state);
	public T putFinishedGameState(VictoryGameState state);
}

