package pfj.thedrake.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import pfj.thedrake.media.GameStateMedia;

public class MiddleGameState extends BaseGameState {
		
	public MiddleGameState(
			Board board, 
			TroopStacks troopStacks,
			BothLeadersPlaced leaders,
			PlayingSide sideOnTurn) { 
		super(
				board, 
				troopStacks,
				leaders,  				 
				sideOnTurn);
	}
	
	@Override
	public BothLeadersPlaced leaders() {
		return (BothLeadersPlaced)super.leaders();
	}
	
	@Override
	public List<Move> allMoves() {
            List<Move> moves = new LinkedList<>();
            
            moves.addAll(stackMoves());
            
            for(Tile it : board()){
                if(it.hasTroop()){
                    if(it.troop().side() == sideOnTurn()){
                        moves.addAll(boardMoves(it.position()));
                    }
                }
            }
            return moves;
	}	
	
	@Override
	public List<Move> boardMoves(TilePosition position) {		
            List<Move> moves = new ArrayList<>();
            Troop tmpTroop = board().tileAt(position).troop();
            // access to board at position. Check for troops changes from position.
            for(BoardChange it : tmpTroop.changesFrom(position, board())){
                moves.add(new BoardMove(this, it));
            }
            return moves;
	}
	
	@Override
	public List<Move> stackMoves() {
            List<Move> moves = new ArrayList<>();
            
            for(Tile tile : board()){
                if(canPlaceFromStack(tile.position())){
                    moves.add(new PlaceFromStack(this, tile.position()));
                }
            }

//            for(int i = 0; i < board().dimension(); ++i){
//                for(int j = 0; j < board().dimension(); ++j){
//                    if(canPlaceFromStack(target))
//                }
//            }
//            
            return moves;
	}
	
	@Override
	public boolean isVictory() {
		return false;
	}
		
	public boolean canPlaceFromStack(TilePosition target) {
            if(!board().contains(target)) return false;
            if(board().tileAt(target).hasTroop()) return false;
            
            return isGoodSideNeighbour(target, 0, -1)
                    || isGoodSideNeighbour(target, 0, 1)
                    || isGoodSideNeighbour(target, -1, 0)
                    || isGoodSideNeighbour(target, 1, 0);
	}
        
        // check if tile is there, has troop and is on good side
        private boolean isGoodSideNeighbour(TilePosition origin, int x, int y){
            return board().contains(origin.step(x,y))
                    && board().tileAt(origin.step(x,y)).hasTroop()
                    && board().tileAt(origin.step(x, y)).troop().side() == sideOnTurn();
        }

	public MiddleGameState placeFromStack(TilePosition target) {
		Troop troop = troopStacks().peek(sideOnTurn());
		return new MiddleGameState(
				board().withTiles(  
					new TroopTile(target, troop)),  
				troopStacks().pop(sideOnTurn()),
				leaders(),
				sideOnTurn().opposite());
	}

    @Override
    public <T> T putToMedia(GameStateMedia<T> media) {
        return media.putMiddleGameState(this);
    }
}
