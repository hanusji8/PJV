
package pfj.thedrake.game;

public enum PlayingSide{
        BLUE{
            @Override
            public PlayingSide opposite(){
                return ORANGE;
            }
        },
        ORANGE;
        
        public PlayingSide opposite(){
            return BLUE;
        }
        
    }
