
package pfj.thedrake.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CapturedTroops {
    private final List<TroopInfo> blueTroops;
    private final List<TroopInfo> orangeTroops;
    
    // Konstruktor vytvářející prázdné seznamy
    public CapturedTroops(){
        this.blueTroops = new ArrayList<>();
        this.orangeTroops = new ArrayList<>();
    }
    
    private CapturedTroops(CapturedTroops capturedTroops){
        this.blueTroops = new ArrayList<>();
        this.blueTroops.addAll(capturedTroops.blueTroops);
        
        this.orangeTroops = new ArrayList<>();
        this.orangeTroops.addAll(capturedTroops.orangeTroops);
        
    }

    CapturedTroops(PlayingSide side, TroopInfo info) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public CapturedTroops(List<TroopInfo> blueTroops, List<TroopInfo> orangeTroops){
        this.blueTroops = blueTroops;
        this.orangeTroops = orangeTroops;
    }

    // Vrací seznam zajatých jednotek pro daného hráče
    public List<TroopInfo> troops(PlayingSide side){
        if(side == PlayingSide.BLUE){
            return Collections.unmodifiableList(blueTroops);
        }
        return Collections.unmodifiableList(orangeTroops);
    }

    // Přidává nově zajatou jednotku na začátek seznamu zajatých jednotek daného hráče.
    public CapturedTroops withTroop(PlayingSide side, TroopInfo info){
        CapturedTroops newCapturedTroops = new CapturedTroops(this);

        
        if(side == PlayingSide.BLUE){
            newCapturedTroops.blueTroops.add(0,info);
        }
        else{
            newCapturedTroops.orangeTroops.add(0,info);
        }
        return newCapturedTroops;
    }
}
