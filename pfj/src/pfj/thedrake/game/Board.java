
package pfj.thedrake.game;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import pfj.thedrake.media.BoardMedia;

public class Board implements Iterable<Tile>{
    private final int dimension;
    private final Tile[][] boardTiles;
    private CapturedTroops capturedTroops;
    // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru se specefikovanými dlaždicemi.
    // Všechny ostatní dlažice se berou jako prázdné.
    
    // Primární konstruktor
    public Board(int dimension, CapturedTroops captured, Tile... tiles){
        this(dimension, tiles);
        this.capturedTroops = captured;
    }
    
    public Board(int dimension, Tile... tiles){
        this.dimension = dimension;
        this.boardTiles = new Tile[dimension][dimension];
        this.capturedTroops = new CapturedTroops();
        
        for(int i = 0; i < dimension; ++i){
            for(int j = 0; j < dimension; ++j){
                this.boardTiles[i][j] = new EmptyTile(new TilePosition(i,j));
            }
        }
        
        for(Tile item : tiles){
            this.boardTiles[item.position().i][item.position().j] = item;
        }
        
    }
    public Board(int dimension, Tile[][] tiles, Tile... tiles2){
        boardTiles = tiles.clone();
        this.dimension = dimension;
        
        for(int i = 0; i < dimension; ++i){
            boardTiles[i] = tiles[i].clone();
        }
        for(Tile item : tiles2){
            boardTiles[item.position().i][item.position().j] = item;
        }
    }
    /*public Board(Board board){
        this.dimension = board.dimension;
        this.boardTiles = new Tile[dimension][dimension];
        this.capturedTroops = new CapturedTroops();
        
        for(int i = 0; i < dimension; ++i){
            this.boardTiles[i] = new Tile[dimension];
            this.boardTiles[i] = board.boardTiles[i].clone();
        }
        
    }*/

    
    public Board withCaptureAndTiles(TroopInfo info, PlayingSide side, Tile... tiles){
        Board newBoard = withTiles(tiles);
        newBoard.capturedTroops = capturedTroops.withTroop(side, info);
        return newBoard;
    }
    

    // Rozměr hrací desky
    public int dimension(){
        return dimension;
    }
    
    public boolean isTileIn(TilePosition position){
        return !(position.i > dimension-1 || position.i < 0 ||
           position.j > dimension-1 || position.j < 0);
    }

    // Vrací dlaždici na zvolené pozici. Pokud je pozice mimo desku, vyhazuje IllegalArgumentException
    public Tile tileAt(TilePosition position){
        if(!isTileIn(position)) throw new IllegalArgumentException();
        return boardTiles[position.i][position.j];
    }

    // Ověřuje, že pozice se nachází na hrací desce
    public boolean contains(TilePosition... positions){
        boolean isTrue = true;
        for(TilePosition item : positions){
            if(!isTileIn(item)){
                isTrue = false;
                break;
            }
        }
        return isTrue;
    }

    // Vytváří novou hrací desku s novými dlaždicemi z pole tiles. Všechny ostatní dlaždice zůstávají stejné
    public Board withTiles(Tile... tiles){
        Board newBoard = new Board(dimension,this.boardTiles, tiles);
        newBoard.capturedTroops = this.capturedTroops;   
        
        return newBoard;
    }
    
    // Vrací zajaté jednotky
    public CapturedTroops captured(){
        return capturedTroops;
    }

    // Stojí na pozici origin jednotka?
    public boolean canTakeFrom(TilePosition origin){
        return contains(origin) && tileAt(origin).hasTroop();
    }

    /*
     * Lze na danou pozici postavit zadanou jednotku? Zde se řeší pouze
     * jednotky na hrací ploše, nikoliv zásobník, takže se v podstatě
     * pouze ptám, zda dlaždice na pozici target přijme danou jednotku.
     */
    public boolean canPlaceTo(Troop troop, TilePosition target){
        return contains(target) && tileAt(target).acceptsTroop(troop);
    }

    // Může zadaná jednotka zajmout na pozici target soupeřovu jednotku?
    public boolean canCaptureOn(Troop troop, TilePosition target){
        return contains(target) && tileAt(target).hasTroop() 
                && tileAt(target).troop().side() != troop.side();
    }

    /*
     * Stojí na políčku origin jednotka, která může udělat krok na pozici target
     * bez toho, aby tam zajala soupeřovu jednotku?
     */
    public boolean canStepOnly(TilePosition origin, TilePosition target){
        return contains(origin, target) && tileAt(origin).hasTroop() 
                && !tileAt(target).hasTroop();
    }

    /*
     * Stojí na políčku origin jednotka, která může zůstat na pozici origin
     * a zajmout soupeřovu jednotku na pozici target?
     */
    public boolean canCaptureOnly(TilePosition origin, TilePosition target){
        return contains(origin, target) && tileAt(origin).hasTroop()
                && tileAt(target).hasTroop() && tileAt(origin).troop().side() != tileAt(target).troop().side();
    }

    /*
     * Stojí na pozici origin jednotka, která může udělat krok na pozici target
     * a zajmout tam soupeřovu jednotku?
     */
    public boolean canStepAndCapture(TilePosition origin, TilePosition target){
        return contains(origin, target) && tileAt(origin).hasTroop() && tileAt(target).hasTroop()
                && tileAt(origin).troop().side() != tileAt(target).troop().side();
    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target bez toho, aby zajala soupeřovu jednotku.
     */
    public Board stepOnly(TilePosition origin, TilePosition target){
        Troop attacker = tileAt(origin).troop();
        
        return withTiles(new EmptyTile(origin), new TroopTile(target, attacker.flipped()));
        
    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target, kde zajala soupeřovu jednotku.
     */
    public Board stepAndCapture(TilePosition origin, TilePosition target) {
        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();

        return withCaptureAndTiles(
          targetTroop.info(),
          targetTroop.side(),
          new EmptyTile(origin),
          new TroopTile(target, attacker.flipped()));
      }
    /*
     * Nová hrací deska, ve které jednotka zůstává stát na pozici origin
     * a zajme soupeřovu jednotku na pozici target.
     */
    public Board captureOnly(TilePosition origin, TilePosition target){
        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();
        
        return withCaptureAndTiles(
                targetTroop.info(),
                targetTroop.side(),
                new EmptyTile(target),
                new TroopTile(origin, targetTroop.flipped()));
    }

    @Override
    public Iterator<Tile> iterator() {
        return new BoardIterator();
    }
    
    private class BoardIterator implements Iterator<Tile>{
        private int i;
        private int j;
        private boolean last;
        public BoardIterator(){
            i = 0;
            j = 0;
            last = false;
        }

        @Override
        public boolean hasNext() {
            if(last)
                return false;
            if(i+1 == dimension){
                if(j+1 == dimension){
                    last = true;
                }
            }
            return true;
        }

        @Override
        public Tile next() {
            int tmpI = i;
            int tmpJ = j;
            ++i;
            if(i == dimension){
                ++j;
                i = 0;
                if(j == dimension){
                    j = 0;
                }
            }
            return boardTiles[tmpI][tmpJ];
        }
        
    }
    public <T> T putToMedia(BoardMedia<T> media) {
        return media.putBoard(this);
    }


}
