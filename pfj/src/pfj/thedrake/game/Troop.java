
package pfj.thedrake.game;

import java.util.LinkedList;
import java.util.List;


public class Troop {
    
    private final TroopInfo info;
    private final PlayingSide side;
    private final TroopFace face;
    // Konstruktor
    public Troop(TroopInfo info, PlayingSide side, TroopFace face){
        this.info = info;
        this.side = side;
        this.face = face;
    }
    
    // Vytvoří jednotku lícem nahoru
    public Troop(TroopInfo info, PlayingSide side){
        this(info, side, TroopFace.FRONT);
    }

    // Všechny změny desky, které může jednotka provést na desce board, pokud stojí na pozici pos.
    public List<BoardChange> changesFrom(TilePosition pos, Board board) {
        List<BoardChange> changes = new LinkedList<>();
        List<TroopAction> actions = this.info.actions(face);
        
        for(TroopAction it : actions){
            List<BoardChange> tmpChanges = it.changesFrom(pos, side, board);
            
            changes.addAll(tmpChanges);
        }
        
        return changes;
    }

    // Info o jednotce
    public TroopInfo info(){
        return info;
    }

    // Barva, za kterou jednotka hraje
    public PlayingSide side(){
        return side;
    }

    // Kterou stranou je jednotka otočena nahoru
    public TroopFace face(){
        return face;
    }

    // Pivot té strany, kterou je zrovna jednotka otočena nahoru
    public Offset2D pivot(){
        return info.pivot(face);
    }

    // Vytvoří jednotku, která má stejné vlastnosti jako tato, jen je otočena druhou stranou nahoru.
    public Troop flipped(){
        if(face == TroopFace.BACK) return new Troop(info, side, TroopFace.FRONT);
        return new Troop(info, side, TroopFace.BACK);
    }


}
