
package pfj.thedrake.game;

import java.util.List;
import static pfj.thedrake.game.TroopFace.FRONT;


public class TroopInfo {
    private final String name;
    private final Offset2D frontPivot, backPivot;
    
    private final List<TroopAction> frontActions;
    private final List<TroopAction> backActions;
    // Konstruktor
    
        public TroopInfo(String name, Offset2D frontPivot, Offset2D backPivot, List<TroopAction> frontActions, List<TroopAction> backActions){
        this.name = name;
        this.frontPivot = frontPivot;
        this.backPivot = backPivot;
        this.frontActions = frontActions;
        this.backActions = backActions;
    }
    
    // Konstruktor, který nastaví lícový i rubový pivot na stejnou hodnotu
    public TroopInfo(String name, Offset2D pivot, List<TroopAction> frontActions, List<TroopAction> backActions){
        this(name, pivot, pivot, frontActions, backActions);
    }

    // Konstruktor, který nastaví lícový i rubový pivot na hodnotu [1, 1]
    public TroopInfo(String name, List<TroopAction> frontActions, List<TroopAction> backActions){
        this(name, new Offset2D(1,1), new Offset2D(1,1), frontActions, backActions);
    }
    
    public List<TroopAction> actions(TroopFace face){
        if(face == FRONT){
            return frontActions;
        }
        else{
            return backActions;
        }
    }

    // Vrací jméno
    public String name(){
        return name;
    }

    // Vrací pivot na zadané straně jednotky
    public Offset2D pivot(TroopFace face) {
        if(face == FRONT){
            return frontPivot;
        }
        return backPivot;
                
    }
}
