/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfj.thedrake.game;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author hany
 */
public class StrikeAction implements TroopAction{
    
    private final Offset2D direction;
    
    public StrikeAction(int dirX, int dirY){
        this.direction = new Offset2D(dirX, dirY);
    }

    @Override
    public List<BoardChange> changesFrom(TilePosition origin, PlayingSide side, Board board) {
        TilePosition target = origin.stepByPlayingSide(direction, side);
        
        if(board.canCaptureOnly(origin, target)){
            return Collections.singletonList(new CaptureOnly(board, origin, target));
        }
        
        return Collections.emptyList();
    }
    
}
