
package cviceni02;

/**
 * @author hanusji8
 */
public class Person {
    private final String name;
    private final int birthYear;
    
    public Person(String name, int birthYear){
        this.name = name;
        this.birthYear = birthYear;
    }
    
    public int age(){
        return 2017 - birthYear;
    }
    
    public String getName(){
        return name;
    }

}
