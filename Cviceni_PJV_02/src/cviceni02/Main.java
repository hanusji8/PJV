
package cviceni02;

/**
 * @author hanusji8
 */
public class Main {

    public static void main(String[] args) {
        Person person = new Person("Jirka", 1994);
        System.out.printf("%s: = %d%n", person.name, person.birthYear);
    }
    
}
